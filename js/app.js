// Création de données 

var creation = (pers, livres) => {

    var a = []
    for (var p = 1; p <= pers; p++) {
	a['p' + p] = []
	for (var l = 1; l <= livres; l++){
	    if(Math.random() < 0.5)
		a['p' + p]['L' + l] = 6 * Math.random() | 0
	}
	}
	return a
}

// Utilisateurs similaires

// On cherche à calculer le score de similarité entre une personne (par exemple P1) et une autre (par exemple P2)

// Pour les livres lus par ces 2 personnes, on regarde la distance Euclidienne entre leurs notes.

//On veut un score levé quand la distance est petite

var euc_sim = (p1, p2, pref) => {

	var commun = Object.keys(pref[p1]).filter(k => pref[p2].hasOwnProperty(k))
	if(commun.length == 0) return 0
	var carres = commun.reduce((a,v) => a + (pref[p1][v]) ** 2,0)
	return 1/(1+Math.sqrt(carres))

}

// Recommandations

var recommande = (pers, pref, fnct) => {

	var totaux = [];
	var sommeSim = [];
	for (var p in pref){
		if(p == pers) continue
        var sim = fnct(pers, p, pref)
		if(sim == 0) continue
		for (var l in pref[p]) {
			if(!pref[pers].hasOwnProperty(l)) {
				totaux[l] = (totaux[l] || 0) + sim
			}
		}
	}
	return Object.entries(totaux).map(([l]) => [l])

}


$( document ).ready(function() {

    let personnes = creation(10, 10)
    let recom = recommande('p1', personnes, euc_sim)
    let table = ""
    let liste = "<ul class='list-group col-sm-4'>"
    for (const [key, value] of Object.entries(personnes)) {
        table += "<p> L'adherent " + key[1] + (key[2] ? key[2] : "") + " a lu les livres suivants : </p>"
        table += "<table class='table table-bordered col-sm-6'><thead><tr><th scope='col'>Livre</th><th scope='col'>Note</th></tr><thead>"
        for([l, n] of Object.entries(value)){
            table += "<tr><td>" + "Livre " + l[1] + (l[2] ? l[2] : "") + "</td><td>" + n + "</td></tr>"
        }
        table += "</table>"
      }
      
      $("#res").html(table).hide()
      $( "#initusers" ).click(function() {
        $( "#res" ).fadeIn( "slow", function() {
            $("#final").removeAttr("disabled");
        });
      });
      
      liste += "<li class='list-group-item'> Résultas de la recherche pour l'adherent 1, nous vous suggérons les livres suivants : </li>"
      for (const [key, value] of Object.entries(recom)){
        for (const [k, l] of Object.entries(value)){
            liste += "<li class='list-group-item list-group-item-success'>" + "Livre " + l[1] + (l[2] ? l[2] : "") + "</li>"
        }
      }
      liste += "</ul>"
      
      $("#result").html(liste).hide()
      $( "#final" ).click(function() {
        $( "#result" ).fadeIn( "slow");
      });
      
  });
      


