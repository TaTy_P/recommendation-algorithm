// Création de données 

var creation = (pers, livres) => {

    var a = {}
    for (var p = 1; p <= pers; p++) {
	a['p' + p] = {}
	for (var l = 1; l <= livres; l++){
	    if(Math.random() < 0.5)
		a['p' + p]['L' + l] = 6 * Math.random() | 0
	}
	}
	return a
}

// Utilisateurs similaires

// On cherche à calculer le score de similarité entre une personne (par exemple P1) et une autre (par exemple P2)

// Pour les livres lus par ces 2 personnes, on regarde la distance Euclidienne entre leurs notes.

//On veut un score levé quand la distance est petite

var euc_sim = (p1, p2, pref) => {

	var commun = Object.keys(pref[p1]).filter(k => pref[p2].hasOwnProperty(k))
	if(commun.length == 0) return 0
	var carres = commun.reduce((a,v) => a + (pref[p1][v]) ** 2,0)
	return 1/(1+Math.sqrt(carres))

}

// Recommandations

var recommande = (pers, pref, fnct_sim = eucl_sim) => {

	var totaux = {};
	var sommeSim = {};
	for (var p in pref){
		if(p == pers) continue
		var sim = fnct_sim(pers, p, pref)
		if(sim == 0) continue
		for (var l in pref[p]) {
			if(!pref[pers].hasOwnProperty(l)) {
				totaux[l] = (totaux[l] || 0) + sim
			}
		}
	}
	return Object.entries(totaux).map(([l,v]) => [l, v / 	sommeSim[l]]).sort((a,b) => b[1] - a[1])

}






