<?php

$servername = 'http://35.210.121.237:9000/index.php';
$username = 'masterdb';
$password = 'Azerty77';

//Connexion à la base de données :

try{
    $conn = new PDO(mysql:host=$servername;dbname=biblio,$username,$password);
    //On définit le mode d'erreur de PDO sur Exception
    $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
    echo 'Connexion réussie';
}

$reponse = $bdd->query('SELECT * From Emprunts');

// Fonction qui calcule la distance euclidienne entre deux adhérents a1 et a2 :

function distance_euclidienne($a1,$a2){
    return array_sum(array_map(function($donnees[$a1]['Livre']['Note'], $donnees[$a2]['Livre']['Note']){
        return abs($donnees[$a1]['Livre']['Note']-$donnees[$a2]['Livre']['Note']) ** 2;
    }, $a1, $a2
    )
    ) ** (1/2);
}

function euclid_sim($a1, $a2, $reponse){

    //Dans un premier temps on regarde quels sont les éléments communs entre a1 & a2 :
    $livres_lus_par_a1 = array();
    $livres_lus_par_a2 = array();
    //On remplie les deux tableaux :
    while($donnees = $reponse->fetchAll()){
        //Liste des livres lus par le premier adherent
        $livres_lus_par_a1[$a1] = $donnees[$a1]['Livre'];
        //Liste des livres lus par le deuxième adherent
        $livres_lus_par_a2[$a2] = $donnees[$a2]['Livre'];
    }
    //Dans la variable commun on trouvera uniqument les livres en commun entre a1 et a2
    $commun = array_intersect($livres_lus_par_a1,$livres_lus_par_a2);
    if(count($commun)==0){
        return 0;
    }
    return 1/(1+distance_euclidienne($a1,$a2));
}

function recommandation($personne, $reponse){

    $totaux = array();
    $sommeSim = array();
    $donnees = $reponse->fetchAll();
    foreach($donnees as $p){
        //On parcours la liste des personnes dans la base de données
        if ($p == $personne){
            continue;
        }
        $sim = euclid_sim($personne, $p, $donnees);
        if($sim == 0){
            continue;
        }
        foreach($donnees[$personne] as $l){
            if($donnees[$personne]['Livre']!= $l){
                $totaux[$l] = ($totaux[$l] || 0) + sim * $donnees['Note'];
                $sommeSim[$l] = ($sommeSim[$l] || 0 ) + sim;
            }
        }
    }

  

        }
    }
}

$reponse ->closeCursor();

?>