<?php
 
  class Adherent
  {
    // Attributes

    public id_Adherent;
    private $nom;
    private $prenom;
    private $dateDeNaissance;
    private $sexe;
    private $email;
    private $dateAdhesion;

    // Constructor :

    public function __construct($id, $n, $p, $dateN, $s, $e, $dateA){
        
        $this->_id_Adherent = $id;
        $this->_nom = $n;
        $this->_prenom = $p;
        $this->_dateNaissance = $dateN;
        $this->_sexe = $s;
        $this->_email = $e;
        $this->_dateAdhesion = $dateA;
    }

    // Methods getters & setters

    public function __getName(){
        return $this->_nom;
    }

    public function __setName($name){
        $this->_nom = $name;
    }
    
    public function __getPrenom(){
        return $this->_prenom;
    }
    
    public function __setPrenom($prenom){
        $this->_prenom = $prenom;
    }

    public function __getdateNaissance(){
        return $this->_dateNaissance;
    }

    public function __setDateNaissance($dateN){
        $this->_dateNaissance = $dateN;
    }

    public function __getSexe(){
        return $this->_sexe;
    }

    public function __setSexe($sexe){
        $this->_sexe = $sexe;
    }

    public function __getEmail(){
        return $this->_email;
    }

    public function __setEmail($email){
        $this->_email = $email;
    }

    public function __getDateAdhesion(){
        return $this->_dateAdhesion;
    }

    public function __setDateAdhesion($date){
        $this->_dateAdhesion = $date;
    }
  }
 
?>