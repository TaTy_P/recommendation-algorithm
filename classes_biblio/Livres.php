<?php
 
  class Livres
  {
    // Attributes

    public $ref_livre;
    private $titre;
    private $auteur;
    private $categorie;
 
    // Constructor 

    public function __construct($ref, $titre, $auteur, $categorie){

        $this->_ref_livre = $ref;
        $this->_titre = $titre;
        $this->_auteur = $auteur;
        $this->_categorie = $categorie;
    }
 
    // Methods getters & setters

    public function __getTitre(){
        return $this->_titre;
    }

    public function __setTitre($titre){
        $this->_titre = $titre;
    }

    public function __getAuteur(){
        return $this->_auteur;
    }

    public function __setAuteur($auteur){
        $this->_auteur = $auteur;
    }

    public function __getCategorie(){
        return $this->_categorie;
    }

    public function __setCategorie(){
        $this->_categorie = $categorie;
    }
  }
 
?>