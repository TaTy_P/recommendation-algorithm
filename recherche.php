<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
    integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
    integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    
    <title> Recherche </title>
</head>
<style>
h3, h6, button{
  margin-left: 20%;
  margin-top: 3%;
  margin-bottom: 3%;
}
table, p, ul{
  margin-left: 20%;
}
#result{
  margin-bottom: 10%;
}
#text-recherche-page{
  margin-top: 20px;
  margin-left: 30px;

}
</style>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php">Home</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item">
        <a class="nav-link" href="#" target="_blank"> # <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a href="#" target="_blank" class="nav-link" href="#"> #</a>
      </li>

    </ul>
    
</nav>

    <div class="" id="text-recherche-page">
      <p><span class="typeWriter" data-text='["Laissez vous guider ! Nous recherchons des livres à votre goût, notre algorithme vous connaît mieux que vous !!! "]'></span></p>
    </div>

    <h3> Simulation  de recommandation d'un livre en utlilisant l'IA </h3>
    <button id="initusers" type="button" class="btn btn-primary" > Initialiser la base d'adhérents </button>
    <h6> Etape 1 : Initialisation de la base de données des adhérents (10 adhérents pour cette demo) </h6>

    <div id="res"></div>
    <button id="final" type="button" class="btn btn-success" disabled>Resultats</button>
    <h6> Etape 2 : Etude comparative avec les autres adhérents et affichage des propositions </h6>
    <div id="result"></div>


    <!-- JS, Popper.js, and jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src ="https://cdn.jsdelivr.net/npm/typelighterjs/typelighter.min.js"></script>
    <script src="js/app.js"></script>

</body>

</html>